// Introduction to Express JS

/*

	Express 
		- open-source software under the MIT License
		- a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications

	Web Framework
		- a set of componets designed to simplify the web dev process
		- just like bootstrap is doing in CSS 
		- allows devs to focus on the business logic of their app

		// comes in two forms:
		1. Opinionated WEB Framework
			- the framework dictates how it should be used by the dev
			- speeds up the statup process of the app development
			- Enforces best parctices for the framework's use case
			- Cons: lack of flexibility, could be a drawback when apps's needs are not aligned with the framework's application

		2. Unopinionated WEB Framework
			- dev dictates how to use the framework
			- offers "flexibility" to create an applicatino unbound by any use case
			- no "right way" of structuring an application, meaning, as long as it is working and no bugs, its all goods
			- Cons: Abundace of options may be overwhelming

		// Advantages: over plain Node.js
				- simplicity makes it easy to learn and use
				- offers ready to use components for the most common web dev needs
				- adds easier routing and processing of HTTP Methods

at terminal:

	npm init 
		- this is to initialize the NPM, to get all the built in componets of the express.js
		// output: 

			package name: (d1) 
			version: (1.0.0) 
			description: 
			entry point: (index.js) 
			test command: 
			git repository: 
			keywords: 
			author: 
			license: (ISC) 
			About to write to /Users/louies/Documents/dequina-ralph/b144/s29/d1/package.json:

			{
			  "name": "d1",
			  "version": "1.0.0",
			  "description": "",
			  "main": "index.js",
			  "scripts": {
			    "test": "echo \"Error: no test specified\" && exit 1",
			    "start": "nodemon index.js"
			  },
			  "author": "",
			  "license": "ISC"
			}


			Is this OK? (yes) 

				then package.json will shown on the folder

		then install express and mongoose:
			npm install express mongoose

		NOTE: .gitignore (to be typed in Terminal)
			open the file, and type the <filename> of all files that you dont want to be pushed in Git.

			and when cloning, just do "npm install" in  terminal, to install again the needed modules


*/

